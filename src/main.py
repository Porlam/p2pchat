from picotui.picotui.context import Context
from picotui.picotui.screen import Screen
from picotui.picotui.widgets import *
from picotui.picotui.defs import *
import socket, ssl, os
from time import sleep
from multiprocessing import Process

with Context():
    messagelabel = WLabel("aaaa", w=48)
    sock = 1234
    port = 51008
    labels = []
    msgqueue = []
    txt = ""
    rcvp = 1234
    context = 1234
    #lock = threading.Lock()
    
    def client():
        context = ssl.create_default_context()
        context = ssl.SSLContext(ssl.PROTOCOL_TLS_SERVER)
        context.verify_mode = ssl.CERT_NONE 
        context.check_hostname = False
        soc = context.wrap_socket(socket.socket(socket.AF_INET), server_hostname="server")
        soc.connect(("server", port))
        sock = soc
        rcvp = Process(target=recvmsg, args=(sock,))
        rcvp.start()
        return sock

    def server():
        context = ssl.create_default_context(ssl.Purpose.CLIENT_AUTH)
        context.load_cert_chain(certfile="keys/server.crt", keyfile="keys/server.key")
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.bind(("0.0.0.0", port))	# 指定したホスト(IP)とポートをソケットに設定
        s.listen(1)					 # 1つの接続要求を待つ
        oldsock, addr = s.accept()		  # 要求が来るまでブロック
        soc = context.wrap_socket(oldsock, server_side=True)
        sleep(1)
        sock = soc
        rcvp = Process(target=recvmsg, args=(sock,))
        rcvp.start()
        return sock

    def recvmsg(recvsoc: socket):
        while(1):
            data = recvsoc.recv(1024)
            if data.decode("utf-8") == "/Bye":				# qが押されたら終了
                recvsoc.shutdown(socket.SHUT_RDWR)
                recvsoc.close()
                exit()
            if data.decode("utf-8") != "":
                msgqueue.append(data.decode("utf-8"))
                if len(msgqueue) >= 10:
                    msgqueue.pop(0)
                msgtxtchange()
                print(textbox.t)
            sleep(0.01)

    def msgboxredraw():
        for i in labels:
            i.redraw()

    def msgtxtchange():
        for i in range(0,len(msgqueue)):
            labels[i].t = msgqueue[i]
        msgboxredraw()

    def sendmsg(sendsoc,msg):
        data = msg
        sendsoc.send(bytes(data, 'utf-8'))	

    Screen.attr_color(C_WHITE, C_BLUE)
    Screen.cls()
    Screen.attr_reset()

    dialog = Dialog(5, 5, 50, 12)

    textbox = WTextEntry(46, "")
    dialog.add(2, 1, textbox)
    for i in range(0, 10):
        msglabel = WLabel("", w=48)
        
        labels.append(msglabel)
        dialog.add(2, i + 2, msglabel)
    OKbutton = WButton(8, "Exit")
    dialog.add(10, 16, OKbutton)
    OKbutton.finish_dialog = ACTION_OK
    if os.environ["MODE"]=="server":
        sock = server()
    if os.environ["MODE"]=="client":
        sock = client()

    def loop():
        dialog.redraw()
        while True:
            key = dialog.get_input()
            if key is None:
                continue
            res = dialog.handle_input(key)
            if key == KEY_ENTER:
                txt = textbox.get()
                if len(msgqueue) >= 10:
                    msgqueue.pop(0)
                msgqueue.append(txt)
                msgtxtchange()
                textbox.set("")
                textbox.t = ""
                textbox.col = 0
                textbox.margin = 0
                textbox.redraw()
                sendmsg(sock, txt)
            if res is not None and res is not True:
                return res
    res = loop()
