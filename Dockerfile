FROM python:3
WORKDIR /src/
RUN apt-get update && apt-get upgrade -y && apt-get install tcpdump iproute2 -y
CMD [ "tail", "-f" ]