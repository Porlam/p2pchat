# p2pchat
Python製のP2Pチャットです
## 使っているライブラリ
* Picotui
## 使い方
※ターミナルが2枚必要です

`docker-compose build` を実行してから `docker-compose up -d` をします

### サーバー側(先に起動させてください)

`docker exec -it p2pchat-server-1 python main.py`

でチャットサーバー兼クライアントが立ち上がります。
起動すると青い画面が出ますが、クライアント側を立ち上げると、チャット画面が出ます。
### クライアント側

`docker exec -it p2pchat-client-1 python main.py`

でクライアントが立ち上がります。

サーバーとつながると、チャット画面が出ます。